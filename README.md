# Exploring, modeling & visualizing reported case data

I'm not an epidemiologist or a virologist and just exploring data, so I will not draw conclusions on that level nor base behavior or policy upon these explorations and neither should you. Even within a country the data is heavily biased, not in the least because of testing and measurement policies changing over time!